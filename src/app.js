import React, {useState, useEffect } from 'react';
import config from 'visual-config-exposer'


const App = () => {
  const lock_des = "./assets/lock1.png"
  const [lock, setLock] = useState(lock_des)
  const lock_des2 = "./assets/lock2.png" 
  const Image = config.settings.Image
  const Price = config.settings.Price
  useEffect(() => {
 document.getElementsByClassName('container')[0].style.backgroundImage = "url("+ Image + ")"

  })

 function unblur() {
    setTimeout(() => {
        document.getElementsByClassName('container')[0].style.filter = 'blur(0px) ';
    document.getElementsByClassName('Main')[0].style.visibility = 'hidden'
    document.getElementsByTagName('img')[0].style.display='none'
    }, 600);
    document.getElementsByTagName('img')[0].classList.add('animat')
    setLock(lock_des2)
   }
 
  return (
    <div className='App'>
 
<div className='container'>
    </div>
    <img  src={lock} alt=''/> 
<div className='Main'>
    <h1>{config.settings.Headline}</h1>
  <p>{config.settings.Description}</p>
    <button className='class2' onClick={unblur}>Unlock (${Price})
    
    </button>
     </div>
    </div>
    
  );
};

export default App;
